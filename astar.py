import csv
import math
import numpy as np
import matplotlib.pyplot as plt
from math import cos, sin, radians, pi
import plotly.express as px

start = (4, 0)
end = (9, 9)
matrix = np.matrix([[0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]])


def file_read(f):
    """
    Reading LIDAR laser beams (angles and corresponding distance data)
    """
    measures = [line.split(";") for line in open(f)]
    angles = []
    distances = []
    for measure in measures:
        if float(measure[1]) >= 1000 or float(measure[0]) < 90 or float(measure[0]) > 270:
            continue
        angles.append(float(measure[0]))
        distances.append(float(measure[1]))
    angles = np.array(angles)
    distances = np.array(distances)
    return angles, distances


ang, dist = file_read("data0.csv")
angl = []
for a in ang:
    angl.append(radians(a))

#    print(ang)
ox = np.cos(np.array(angl)) * dist
oy = np.sin(np.array(angl)) * dist
plt.xlim(0, 1000)
plt.scatter(-ox, -oy, s=2, alpha=0.5)
plt.show()
Tox = []
Toy = []
for i in ox:
    Tox.append(int((-i) / 100))
for j in oy:
    Toy.append(int((j + 1000) / 200))

tab = []
for i in range(len(Tox)):
    matrix[Toy[i], Tox[i]] = 1


# matrix = np.matrix(
#   [[0., 0., 0., 0., 0.], [0., 0., 1., 0., 0.], [0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], [0., 0., 0., 0., 0.]])


class Node:
    def __init__(self, coord, dist, origin):
        self.coord = coord
        self.dist = dist
        self.origin = origin


def astar(maze, start, end):
    ## TODO: Implement A start algorithm
    mat = maze
    st_node = Node(start, 0, None)
    end_node = Node(end, math.inf, None)
    closed_list = []  # 1 noeud deja visité
    opened_list = [st_node]  # noeuds a visité
    while len(opened_list) > 0:
        current, opened_list = getNearest(end, opened_list)
        closed_list.append(current)
        if current.coord == end_node.coord:
            return rebuildPath(current)
        children = []
        for pos in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
            next_pos = (current.coord[0] + pos[0]), (current.coord[1] + pos[1])
            if not validNeighbor(next_pos, mat, current):
                continue
            print(next_pos)
            child = Node(next_pos, current.dist + 1, current)
            children.append(child)
        for child in children:
            # si il existe le meme node avec une dist inférieur dans open_list
            if check(child, closed_list, opened_list):
                continue
            opened_list.append(child)


def check(child, closed_list, opened_list):
    for closed_node in closed_list:
        if child.coord == closed_node.coord:
            return True
    for opened_node in opened_list:
        if child.coord == opened_node.coord and child.dist > opened_node.dist:
            return True

    return False


def validNeighbor(next_pos, mat, current):
    if next_pos[0] < 0 or next_pos[1] < 0 or next_pos[0] >= mat.shape[0] or \
            next_pos[1] >= mat.shape[1]:
        return False
    if mat[next_pos[0]][next_pos[1]] == 0.:
        return True
    else:
        return False


def getNearest(end, opened_list):
    oldDist = math.inf
    nearNode = None
    index = 0
    for v, i in enumerate(opened_list):
        dist = math.pow(i.coord[0] - end[0], 2) + math.pow(i.coord[1] - end[1], 2)
        if dist < oldDist:
            oldDist = dist
            nearNode = i
            index = v
    opened_list.pop(index)

    return nearNode, opened_list


def rebuildPath(current):
    listCoord = []
    listCoord.append(current.coord)
    previousNode = current.origin

    while previousNode is not None:
        listCoord.append(previousNode.coord)
        previousNode = previousNode.origin

    return listCoord[::-1]


def verify(maze, start, end, path):
    if path[0][0] != start[0] or path[0][1] != start[1] or path[-1][0] != end[0] or path[-1][1] != end[1]:
        print('Start or end incorrect')
        return False
    last = None
    for pos in path:
        if maze[pos] == 1:
            print('Path cross a wall')
            return False
        if last == None:
            last = pos
            continue
        diffX = abs(pos[0] - last[0])
        diffY = abs(pos[1] - last[1])
        if diffX > 1 or diffY > 1 or diffX + diffY > 1:
            print('Path not consecutive')
            return False
        last = pos
    print('Correct path')
    return True


def displayMatrix(mat):
    plt.matshow(mat)
    plt.show()


def displayPath(mat, path):
    if path != None:
        for (y, x) in path:
            mat[y, x] = math.inf
    plt.matshow(mat)
    plt.show()


displayMatrix(matrix)
maze = np.asarray(matrix)
# start = (2, 0)
# end = (4, 3)

path = astar(maze, start, end)
displayPath(matrix, path)
print(verify(maze, start, end, path))